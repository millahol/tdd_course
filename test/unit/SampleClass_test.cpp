/*
 * SampleClass_test.cpp
 *
 *  Created on: Mar 18, 2020
 *      Author: Author
 */

//#include <sstream>
//#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "SampleClass.h"

//using ::testing::InSequence;
//using ::testing::StrEq;
//using ::testing::Return;
using ::testing::Test;
//using ::testing::_;

using ::SampleClass;

class ut_SampleClass: public Test {
protected:
	virtual void SetUp() {
		m_Rc = -1;
	}
	virtual void TearDown() {
	}
protected:
	int m_Rc;
	SampleClass m_Instance;
};

//
// This test verifies the basics of suite by:
// - constructing via SetUp
// - destructing via TearDown
// Running this with Valgrind (too) can show if the suite is
// internally memory-solid, ie does not leak mocks etc.
//

TEST_F(ut_SampleClass, fixture) {
	EXPECT_EQ(-1, m_Rc);
}

TEST_F(ut_SampleClass, construction) {
}

TEST_F(ut_SampleClass, rename_this_test_case) {
	ASSERT_EQ(-1, m_Rc);
}

