/*
Template by Bittium Wireless Oy 2019.

All rights reserved. This software is the confidential and proprietary
information of Bittium. Any use of this software is subject to the conclusion
of a respective license agreement with Bittium. No rights to the software
are granted without such license agreement.
*/

#include <gtest/gtest.h>

int main(int argc, char **argv) {
    int rc = -1;

    ::testing::InitGoogleTest(&argc, argv);
    rc = RUN_ALL_TESTS();
    return rc;
}
